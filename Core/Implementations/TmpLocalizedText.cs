﻿using System.Runtime.CompilerServices;
using CrazyRam.Core.Helpers;
using TMPro;
using UnityEngine;

namespace CrazyRam.Core.Localization
{
    public class TmpLocalizedText : MonoBehaviour
    {
        [SerializeField]
        protected TextMeshProUGUI LocalizationUnit;

        [SerializeField]
        protected string Key;

        private static LocalizationManager _manager;

        protected static LocalizationManager Manager
        {
            [MethodImpl(MethodImplOptions.AggressiveInlining)]
            get => _manager.Ok() ?? (_manager = ReferenceResolver.Instance.Pull<LocalizationManager>());
        }

        public string LocalizationLey
        {
            [MethodImpl(MethodImplOptions.AggressiveInlining)]
            get => Key;
            private set
            {
                Key = value;
                InitText();
            }
        }

        protected void Start()
        {
            InitText();
            LocalizationManager.LocalizationLoaded += InitText;
        }

        protected void OnDestroy()
        {
            LocalizationManager.LocalizationLoaded -= InitText;
        }

        public void Init(string key)
        {
            LocalizationLey = key;
        }

        protected virtual void InitText()
        {
            if (!LocalizationUnit)
                LocalizationUnit = GetComponent<TextMeshProUGUI>();
            var manager = Manager;
            if (manager && !string.IsNullOrEmpty(Key))
                LocalizationUnit.text = manager.GetWord(Key);
        }
    }
}
