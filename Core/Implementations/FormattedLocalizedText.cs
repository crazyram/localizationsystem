﻿using CrazyRam.Core.Helpers;

namespace CrazyRam.Core.Localization
{
    public class FormattedLocalizedText : LocalizedText
    {
        private object[] _words;

        protected override void InitText()
        {
            if (Manager != null && !string.IsNullOrEmpty(Key) && !_words.IsNullOrEmpty())
                LocalizationUnit.text = string.Format(Manager.GetWord(Key), _words);
        }

        public void Init(params object[] words)
        {
            _words = words;
            InitText();
        }

        public void InitWithKey(string key, params object[] words)
        {
            Key = key;
            Init(words);
        }
    }
}
