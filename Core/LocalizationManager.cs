﻿using System;
using System.Runtime.CompilerServices;
using CrazyRam.Core.Helpers;
using CrazyRam.Core.Initialization;
using UnityEngine;
#if CRAZY_MESSAGE_BUS_INCLUDED
using CrazyRam.Core.MessageBus;
#endif

namespace CrazyRam.Core.Localization
{
    public class LocalizationManager : MonoBehaviour, IInitializer
#if CRAZY_MESSAGE_BUS_INCLUDED
        , IMessageReceiver<LocalizationLanguageChangeEvent>,
        IPointerMessageReceiver<LocalizationLoadKeyEvent>
#endif
    {
        [SerializeField]
        private Languages _selectedLanguage = Languages.Russian;

        public Languages SelectedLanguage => _selectedLanguage;

        private LocalizationDatabase _currentLocalization;

        public static event Action LocalizationLoaded;

        public void Init()
        {
            LoadLanguage();
            ReferenceResolver.Instance.Push(this);
        }

        private void LoadLanguage()
        {
            _currentLocalization =
                Resources.Load(_selectedLanguage.ToString(), typeof(LocalizationDatabase)) as LocalizationDatabase;
            if (_currentLocalization != null)
                LocalizationLoaded.Raise();
#if CRAZY_MESSAGE_BUS_INCLUDED
            ClientDispatcher<LocalizationLanguageLoadedEvent>.Default.Raise(
                new LocalizationLanguageLoadedEvent(_selectedLanguage));
#endif
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public string GetWord(string key)
        {
            return _currentLocalization != null ? _currentLocalization.GetWord(key) : string.Empty;
        }

        public void ChangeLanguage(Languages language)
        {
            if (_selectedLanguage == language)
                return;
            _selectedLanguage = language;
            LoadLanguage();
        }

#if CRAZY_MESSAGE_BUS_INCLUDED
        public void OnMessage(in LocalizationLanguageChangeEvent data)
        {
            ChangeLanguage(data.Language);
        }

        public void OnRefMessage(ref LocalizationLoadKeyEvent data)
        {
            data.Value = GetWord(data.Key);
        }
#endif
    }
}
