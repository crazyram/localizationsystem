﻿namespace CrazyRam.Core.Localization
{
    public enum Languages
    {
        Russian = 0,
        English = 1,
        French = 2,
        German = 3,
        Spanish = 4,
        Italian = 5
    }
}
