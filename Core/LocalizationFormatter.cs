﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using UnityEngine;

public static class LocalizationFormatter
{
    public static string[,] SplitTsvGrid(string csvText, int width = -1)
    {
        var lines = csvText.Split("\n"[0]);

        Debug.Log(csvText);
        // finds the max width of row
        if (width <= 0)
            width = lines.Select(SplitTsvLine).Aggregate(0, (current, row) => Mathf.Max(current, row.Length));

        // creates new 2D string grid to output to
        var outputGrid = new string[width + 1, lines.Length + 1];

        for (int y = 0; y < lines.Length; y++)
        {
            var row = SplitTsvLine(lines[y]);
            for (int x = 0; x < row.Length; x++)
            {
                outputGrid[x, y] = row[x];
                Debug.Log(row[x]);
                outputGrid[x, y] = outputGrid[x, y].Replace("\"\"", "\"");
            }
        }

        return outputGrid;
    }

    private static string[] SplitTsvLine(string line)
    {
        Debug.Log(line);
        const char splitter = '	';
        return line.Split(splitter);
    }

    public static string[][] ParseFile(string text)
    {
        using (var reader = new StringReader(text))
        {
            return Parse(reader);
        }
    }

    private static string[][] Parse(TextReader reader)
    {
        var data = new List<string[]>();

        var col = new StringBuilder();
        var row = new List<string>();

        string ln;
        do
        {
            ln = reader.ReadLine();
            if (string.IsNullOrEmpty(ln) || !Tokenize(ln, col, row))
                continue;
            data.Add(row.ToArray());
            row.Clear();
        } while (ln != null);
        return data.ToArray();
    }

    private static bool Tokenize(string s, StringBuilder col, List<string> row)
    {
        int i = 0;

        if (col.Length > 0)
        {
            col.AppendLine(); // continuation

            if (!TokenizeQuote(s, ref i, col))
            {
                return false;
            }
        }

        while (i < s.Length)
        {
            var ch = s[i];
            switch (ch)
            {
                case ',':
                    row.Add(col.ToString().Trim());
                    col.Length = 0;
                    i++;
                    break;
                case '"':
                    i++;
                    if (!TokenizeQuote(s, ref i, col))
                    {
                        return false;
                    }

                    break;
                default:
                    col.Append(ch);
                    i++;
                    break;
            }
        }

        if (col.Length <= 0) return true;
        row.Add(col.ToString().Trim());
        col.Length = 0;

        return true;
    }

    private static bool TokenizeQuote(string s, ref int i, StringBuilder col)
    {
        while (i < s.Length)
        {
            var ch = s[i];
            if (ch == '"')
            {
                if (i + 1 < s.Length && s[i + 1] == '"')
                {
                    col.Append('"');
                    i++;
                    i++;
                    continue;
                }
                i++;
                return true;
            }
            col.Append(ch);
            i++;
        }
        return false;
    }
}
