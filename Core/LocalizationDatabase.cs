﻿using System;
using UnityEngine;
using System.Collections.Generic;
using Unity.Collections;
#if UNITY_EDITOR
using UnityEditor;
#endif

namespace CrazyRam.Core.Localization
{
    [Serializable]
    public class LocalizationDatabase : ScriptableObject, ISerializationCallbackReceiver
    {
        public Dictionary<string, string> Localization;

        [SerializeField]
        private List<string> _keys;

        [SerializeField]
        private List<string> _values;

        private const string KeyNotFound = "KEY_NOT_FOUND";

#if UNITY_EDITOR

        public void Add(string key, string val)
        {
            if (_keys == null)
                _keys = new List<string>();
            if (_values == null)
                _values = new List<string>();
            if (Localization == null)
                Localization = new Dictionary<string, string>();

            if (_keys.Contains(key) && Localization.ContainsKey(key))
            {
                var idx = _keys.IndexOf(key);
                _values[idx] = val;
                Localization[key] = val;
                return;
            }

            _keys.Add(key);
            _values.Add(val);
            Localization.Add(key, val);
        }

        [MenuItem("CrazyRam/Localization/Create")]
        public static void Create()
        {
            var asset = CreateInstance<LocalizationDatabase>();
            AssetDatabase.CreateAsset(asset, "Assets/LocalizationDatabase.asset");
        }

#endif

        public void OnBeforeSerialize()
        {
            if (Localization == null)
                return;
            _keys = new List<string>();
            _values = new List<string>();
            foreach (var pair in Localization)
            {
                _keys.Add(pair.Key);
                _values.Add(pair.Value);
            }
        }

        public void OnAfterDeserialize()
        {
            if (_keys.Count != _values.Count)
                return;

            Localization = new Dictionary<string, string>();
            for (int i = 0; i < _keys.Count; ++i)
                Localization.Add(_keys[i], _values[i]);
        }

        public string GetWord(string key)
        {
            return Localization.TryGetValue(key, out var result) ? result : null;
        }

        public void Clear()
        {
            _keys?.Clear();
            _values?.Clear();
            Localization?.Clear();
        }

        public void LoadRaw(string data)
        {
            //key cell + value cell
            const int tableWidth = 2;

            Clear();
            var grid = LocalizationFormatter.ParseFile(data);
            for (int i = 1; i < grid.Length; i++)
            {
                //key: grid[i][0], value: grid[i][1]
                var line = grid[i];
                if (line.Length >= tableWidth && !string.IsNullOrEmpty(line[0]) && !string.IsNullOrEmpty(line[1]))
                    Localization[line[0]] = line[1];
            }
        }
    }
}
