using System;
using Unity.Collections;

namespace CrazyRam.Core.Localization
{
    public class LocalizationNativeDatabase : IDisposable
    {
        private NativeHashMap<LocalizationKey, IntPtr> _data;

        public bool TryGet(LocalizationKey key, out IntPtr text)
        {
            return _data.TryGetValue(key, out text);
        }

        public void Load()
        {
        }

        public void Dispose()
        {
        }
    }
}
