using System;

namespace CrazyRam.Core.Localization
{
    public readonly struct LocalizationKey : IEquatable<LocalizationKey>
    {
        private readonly int _hash;

        public bool Equals(LocalizationKey other)
        {
            return _hash == other._hash;
        }

        public override bool Equals(object obj)
        {
            return obj is LocalizationKey other && Equals(other);
        }

        public override int GetHashCode()
        {
            return _hash;
        }
    }
}
