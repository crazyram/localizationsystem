﻿public struct LocalizationLoadKeyEvent
{
    public string Key;

    public string Value;

    public LocalizationLoadKeyEvent(string key) : this()
    {
        Key = key;
    }
}
