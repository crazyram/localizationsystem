﻿namespace CrazyRam.Core.Localization
{
    public readonly struct LocalizationLanguageChangeEvent
    {
        public readonly Languages Language;

        public LocalizationLanguageChangeEvent(Languages language)
        {
            Language = language;
        }
    }
}
