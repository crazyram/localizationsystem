﻿namespace CrazyRam.Core.Localization
{
    public readonly struct LocalizationLanguageLoadedEvent
    {
        public readonly Languages Language;

        public LocalizationLanguageLoadedEvent(Languages language)
        {
            Language = language;
        }
    }
}
