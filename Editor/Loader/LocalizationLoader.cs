﻿using System.Collections.Generic;
using System.Linq;
using CrazyRam.Core.Localization;
using CrazyRam.Core.Utils;
using UnityEditor;
using UnityEngine;
using UnityEngine.Networking;
using Object = UnityEngine.Object;

namespace CrazyRam.Editor
{
    public class LocalizationLoader : EditorWindow
    {
        private List<LocalizationCsv> _localization;

        private const string KeyName = "KeyId";

        private const string ListName = "ListId";

        private const float ElementWidth = 100;

        private const float NameWidth = 60;

        private const float LabelWidth = 40f;

        private const string GoogleSheetTsvUrlOld =
            "https://docs.google.com/spreadsheets/d/{0}/export?format=tsv&id=KEY&gid={1}";

        private const string GoogleSheetCsvUrlOld =
            "https://docs.google.com/spreadsheets/d/{0}/export?format=csv&id=KEY&gid={1}";

        private const string GoogleSheetTsvUrl2020 =
            "https://docs.google.com/spreadsheets/d/{0}/export?format=tsv&gid={1}";

        private const string GoogleSheetCsvUrl2020 =
            "https://docs.google.com/spreadsheets/d/{0}/export?format=csv&gid={1}";

        [MenuItem("CrazyRam/Localization/LoadWWW")]
        public static void Init()
        {
            var window = GetWindow<LocalizationLoader>();
            window.InitLanguages();
        }

        private void InitLanguages()
        {
            _localization ??= new List<LocalizationCsv>();
            var languages = Resources.FindObjectsOfTypeAll(typeof(LocalizationDatabase));
            OnLocalizationDropped(languages);
        }

        // ReSharper disable once InconsistentNaming
        public void OnGUI()
        {
            EditorGUIUtility.labelWidth = LabelWidth;
            EditorGUIUtility.fieldWidth = LabelWidth;
            EditorGUILayout.LabelField("Drag&Drop localization files from Assets/Resources here if necessary");

            if (_localization == null)
                return;
            for (int i = 0; i < _localization.Count; i++)
            {
                int id = i;
                CrazyEditorHelper.DrawLabel(() => DrawLocalizationBlock(_localization[id]));
            }

            ProcessInput();
        }

        private static void DrawLocalizationBlock(LocalizationCsv localization)
        {
            EditorGUILayout.BeginHorizontal();
            EditorGUILayout.LabelField(localization.Base.name, GUILayout.Width(NameWidth));

            EditorGUI.BeginChangeCheck();
            localization.Key = EditorGUILayout.TextField(localization.Key, GUILayout.Width(ElementWidth * 2));
            if (EditorGUI.EndChangeCheck())
                EditorPrefs.SetString(localization.UrlKey, localization.Key);

            EditorGUI.BeginChangeCheck();
            localization.ListId = EditorGUILayout.TextField(localization.ListId, GUILayout.Width(ElementWidth));
            if (EditorGUI.EndChangeCheck())
                EditorPrefs.SetString(localization.IdKey, localization.ListId);

            if (GUILayout.Button("Load", GUILayout.Width(ElementWidth)))
                LoadByUrl(localization);

            if (GUILayout.Button("Clear", GUILayout.Width(ElementWidth)))
                localization.Base.Clear();
            EditorGUILayout.EndHorizontal();
        }

        private static async void LoadByUrl(LocalizationCsv csv)
        {
            string url = string.Format(GoogleSheetCsvUrl2020, csv.Key, csv.ListId);
            var request = UnityWebRequest.Get(url);
            await request.SendWebRequest();
            if (request.result == UnityWebRequest.Result.ProtocolError)
            {
                Debug.LogError(request.error);
                return;
            }

            csv.LoadSheet(request.downloadHandler.text);
        }

        private void ProcessInput()
        {
            var ev = Event.current;
            switch (ev.type)
            {
                case EventType.DragUpdated:
                    DragAndDrop.visualMode = DragAndDropVisualMode.Copy;
                    break;
                case EventType.DragPerform:
                    OnLocalizationDropped(DragAndDrop.objectReferences);
                    ev.Use();
                    break;
            }
        }

        private void OnLocalizationDropped(Object[] objectReferences)
        {
            foreach (var objRef in objectReferences)
            {
                var db = objRef as LocalizationDatabase;
                if (db == null || _localization.FirstOrDefault(f => f.Base == db) != null)
                    return;
                var csv = new LocalizationCsv
                {
                    Base = db,
                    UrlKey = $"{Application.productName}_{db.name}_{KeyName}",
                    IdKey = $"{Application.productName}_{db.name}_{ListName}"
                };
                csv.FetchUrl();
                _localization.Add(csv);
            }

            EditorUtility.SetDirty(this);
        }
    }
}
