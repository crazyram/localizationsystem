﻿using CrazyRam.Core.Localization;
using UnityEditor;
using UnityEngine;

namespace CrazyRam.Editor
{
	public class LocalizationCsv
	{
		public string Key;

		public string ListId;

		public string UrlKey;

		public string IdKey;

		public LocalizationDatabase Base;

		public void FetchUrl()
		{
			Key = EditorPrefs.GetString(UrlKey);
			ListId = EditorPrefs.GetString(IdKey);
		}

		public void LoadSheet(string text)
		{
			Debug.Log($"Load localization sheet: {Base.name}");
			Base.LoadRaw(text);
			EditorUtility.SetDirty(Base);
		}
	}
}
