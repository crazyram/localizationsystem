﻿using System.IO;
using System.Text;
using CrazyRam.Core.Helpers;
using CrazyRam.Core.Localization;
using UnityEditor;
using UnityEngine;

namespace CrazyRam.Editor
{
    public static class LocalizationToCsv
    {
        [MenuItem("Assets/Export Csv")]
        public static void ExportToCsv()
        {
            var db = Selection.activeObject as LocalizationDatabase;
            if (db == null)
                return;

            var builder = new StringBuilder();
            builder.Append("Key,Value");
            builder.Append("\n");

            foreach (var pair in db.Localization)
            {
                AppendString(builder, pair.Key);
                builder.Append(",");
                AppendString(builder, pair.Value);
                builder.Append(",");
                builder.Append("\n");
            }

            string fileName = $"{Application.dataPath}/{db.name}.csv";
            File.WriteAllText(fileName, builder.ToString(), Encoding.UTF8);
        }

        private static void AppendString(StringBuilder builder, string value)
        {
            if (string.IsNullOrEmpty(value))
                return;
            value = value.Replace("\\n", "\n");
            if (value.IndexOfAny(",\n\"".ToCharArray()) >= 0)
            {
                value = value.Replace("\"", "\"\"");
                builder.AppendFormat("\"{0}\"", value);
            }
            else
                builder.Append(value);
        }

        [MenuItem("Assets/Export Csv", true)]
        public static bool CheckExportToCsv()
        {
            return Selection.activeObject.Ok()?.GetType() == typeof(LocalizationDatabase);
        }
    }
}
